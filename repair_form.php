<?php
include 'connection.php';
if(isset($_POST['hddn_id']) && $_POST['hddn_id'] != ''){//Update
	$id = $_POST['hddn_id'];
	$query = "UPDATE repair_assets SET emp_id='{$_POST['emp_name']}',asset_name='{$_POST['assets_name']}',assignasset_id='{$_POST['assignasset_id']}',remark='{$_POST['remark']}' WHERE `repair_id`='" . $id . "'";
	$result = mysqli_query($conn,$query);
	unset($_POST['hddn_id']);	
	header('Location:repair.php');
}elseif($_GET['id']){//Edit
	$sql = mysqli_query($conn,"SELECT * FROM repair_assets WHERE repair_id = ".$_GET['id']);
	$repair_asset_data = mysqli_fetch_assoc($sql);
}elseif($_POST['submit']){
	$employee_name = $_POST['emp_name'];
	$asset_name = $_POST['assets_name'];
	$remark = $_POST['remark'];
	$assignasset_id = $_POST['assignasset_id'];
	$date = date('Y-m-d H:i:s');
	$query = "INSERT INTO `repair_assets` (`emp_id`, `asset_name`, `assignasset_id`, `remark`, `created`) values ('{$employee_name}','{$asset_name}','{$assignasset_id}','{$remark}','{$date}')";
	$result = mysqli_query($conn,$query);
	unset($_POST['submit']);	
	header('Location:repair.php');
}
	
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
  <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
    <?php
      include 'sidebarmenu.php';
    ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
			<div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php if($_GET['view']!=1 && isset($_GET["id"])){echo 'Edit';}elseif($_GET['view']==1 && isset($_GET["id"])){echo 'View';}else{echo 'Add';}?> Repair asset</h4>
                  <form class="forms-sample" id="form" action="repair_form.php" method="post">
					<input type="hidden" name='hddn_id' value='<?php if($_GET['view']!=1 && isset($_GET["id"])){ echo $_GET["id"]; }; ?>'>
                    <div class="form-group">
						<label for="empname">Employee name</label>
						<select class="form-control" name="emp_name" id="emp_name"  <?php if($_GET['view']==1){echo 'disabled';}?>>
						<?php
							$select_ename = mysqli_query($conn,"SELECT * FROM employee_details WHERE 1");
							echo '<option selected disabled>Select employee</option>';
							while($row = mysqli_fetch_assoc($select_ename)){
								if($row['emp_id'] == $repair_asset_data['emp_id']) {
									echo '<option value="'.$row['emp_id'].'" selected>'.$row['emp_name'].'</option>';	
								}else{
									echo '<option value="'.$row['emp_id'].'">'.$row['emp_name'].'</option>';
								}
							}
						?>
						</select>
					</div>
					<div class="form-group">
						<label for="empname">Assets Name</label>
						<select class="form-control" name="assets_name" id="repaireassets_name"  <?php if($_GET['view']==1){echo 'disabled';}?>>
						<?php
							$select_aname = mysqli_query($conn,"SELECT DISTINCT assets_name FROM assets WHERE 1");
							echo '<option selected disabled>Select Asset</option>';
							while($row = mysqli_fetch_assoc($select_aname)){
								if($row['assets_name'] == $repair_asset_data['asset_name']) {
									echo '<option value="'.$row['assets_name'].'" selected>'.$row['assets_name'].'</option>';	
								}else{
									echo '<option value="'.$row['assets_name'].'">'.$row['assets_name'].'</option>';
								}
							}
						?>
						</select>
					</div>
					<div class="form-group assignid_section">
						<label for="exampleAssetsAssignID">Assign ID</label><br/>
						<input type="text" name="assignasset_id" id="assignasset_id" class="form-control" required autocomplete="off" value="<?php if(isset($repair_asset_data)){echo $repair_asset_data['assignasset_id'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Assign ID"> 
					</div>
					<div class="form-group">
						<label for="">&nbsp;</label>Remark<br/>
						<textarea name="remark" class="form-control" <?php if($_GET['view']==1){echo 'disabled';}?>><?php if(isset($repair_asset_data)){echo $repair_asset_data['remark'];} ?></textarea>	
					</div>
					<?php 
						if($_GET['view']==1){
						echo '<a href="repair.php" class="back-cancel-btn btn btn-light">Back</a>';
						}else{
						if($_GET['id']){
							echo '<button type="submit" name="submit" class="btn btn-primary mr-2">Update</button>';
						}else{
							echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2">Submit</button>';
						}
						echo '<a href="repair.php" class="btn btn-light back-cancel-btn">Cancel</a>';			
						}
					?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>
  <script src="js/custom.js"></script>

</body>

</html>