<?php
include "connection.php"; // Using database connection file  // Using database connection file here

$id = $_GET['id']; // get id through query string

$del = mysqli_query($conn,"DELETE from ms_details where id = " .$id);  // delete query
//print_r("DELETE from ms_details where id = " $id);exit;

if($del)
{
    mysqli_close($conn); // Close connection
    header("location:msList.php"); // redirects to all records page
    exit; 
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
    <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <?php
        include 'sidebarmenu.php';
      ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Microsoft Listing</h4>
                  <a href="msForm.php">Add MS Licence</a>
                  <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Microsoft Licence</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="persons">
                        <?php
                            $sql = "SELECT * FROM `ms_details` WHERE 1";
                            $result = $conn->query($sql);
                            $slno = 1;
                            while($row=$result->fetch_assoc()) {
                                echo "<tr>
                                <td>".$slno++."</td>
                                <td>".$row["ms_id"]."</td>
                                <td><a href='msForm.php?id=".$row["id"]."'>Edit</a> |
                                    <a href='msForm.php?id=".$row["id"]."&view=1'>View</a> |
                                    <a href='msList.php?id=".$row["id"]."'>Delete</a>
                                    </td></tr>";
                            }
                          
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>
  
</body>
</html>



