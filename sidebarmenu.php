<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="dashboard.php">
        <i class="mdi mdi-home menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="assetList.php">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">Assets Listing</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="departmentList.php">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">Department Listing</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="employeeList.php">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">Employee Listing</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="assignEmployeeList.php">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">Assign asset Listing</span>
      </a>
    </li>
      </li>
    <li class="nav-item">
      <a class="nav-link" href="repair.php">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">Repair Listing</span>
      </a>
    </li>
      </li>
    <li class="nav-item">
      <a class="nav-link" href="sophosList.php">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">Sophos Listing</span>
      </a>
    </li>
      </li>
    <li class="nav-item">
      <a class="nav-link" href="msList.php">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">MS Listing</span>
      </a>
    </li>
  </ul>
</nav>