<?php
    session_start();
	include 'connection.php';
	
	if(isset($_POST['hddn_id']) && $_POST['hddn_id'] != ''){//Update
		$id = $_POST['hddn_id'];
		$datetime = date("Y-m-d H:i:s");
		$query = "UPDATE assign_assets SET emp_name='{$_POST['emp_name']}',assets_name='{$_POST['assets_name']}',assets_brand='{$_POST['assets_brand']}',assets_config='{$_POST['assets_config']}',serial_no='{$_POST['serial_no']}',asset_assign_id='{$_POST['asset_assign_id']}',charger_serial_no='{$_POST['charger_serial_no']}',dateof_purchase='{$_POST['dateof_purchase']}',updated_at='{$datetime}' WHERE `assign_id`='" . $id . "'";
		print_r($query);
		$result = mysqli_query($conn,$query);
		unset($_POST['hddn_id']);	
		header('Location:assignEmployeeList.php');
	}elseif($_GET['id']){//Edit
		$sql = mysqli_query($conn,"SELECT * FROM assign_assets WHERE assign_id = ".$_GET['id']);
		$assign_data = mysqli_fetch_assoc($sql);
	}else{//Add
		if($_POST['submit']){
			$department_name = $_POST['department_name'];
			$datetime = date("Y-m-d H:i:s");
			$get_stock_in_use = mysqli_query($conn,"SELECT in_use,id FROM assets WHERE assets_name = '{$_POST['assets_name']}' AND asset_brand = '{$_POST['assets_brand']}' AND asset_config = '{$_POST['assets_config']}'");
			$asset_details = mysqli_fetch_assoc($get_stock_in_use);		
			$in_use_device = $asset_details['in_use']+1;
			$id = $asset_details['id'];			
			$update_query = mysqli_query($conn,"UPDATE assets SET in_use = '".$in_use_device."' WHERE id = ".$id);
			$query = "INSERT INTO `assign_assets` (`emp_name`,`assets_name`,`assets_brand`,`assets_config`,`serial_no`,`asset_assign_id`,`charger_serial_no`,`dateof_purchase`,`created_at`,`updated_at`) values ('{$_POST['emp_name']}','{$_POST['assets_name']}','{$_POST['assets_brand']}','{$_POST['assets_config']}','{$_POST['serial_no']}','{$_POST['asset_assign_id']}','{$_POST['charger_serial_no']}','{$_POST['dateof_purchase']}','{$datetime}','{$datetime}')";
			$result = mysqli_query($conn,$query);
			unset($_POST['submit']);	
			header('Location:assignEmployeeList.php');
		}
	}
	
	?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
  	<?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
		<?php
		include 'sidebarmenu.php';
		?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
		<div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php if($_GET['view']!=1 && isset($_GET["id"])){echo 'Edit';}elseif($_GET['view']==1 && isset($_GET["id"])){echo 'View';}else{echo 'Add';}?> assign asset</h4>
                  <form class="forms-sample" id="form" action="assignEmployeeForm.php" method="post">
					<input type="hidden" name='hddn_id' value='<?php if($_GET['view']!=1 && isset($_GET["id"])){ echo $_GET["id"]; }; ?>'>
                    <div class="form-group">
						<label for="exampleSelectEmployee">Choose Employee</label>
						<select class="form-control" name="emp_name" id="emp_name"  <?php if($_GET['view']==1){echo 'disabled';}?>>
							<?php
							$select_depart = mysqli_query($conn,"SELECT * FROM employee_details WHERE 1");
							while($row = mysqli_fetch_assoc($select_depart)){
								if($assign_data['emp_name'] == $row['emp_id']){
								echo '<option value="'.$row['emp_id'].'" selected>'.$row['emp_name'].'</option>';
								}else{
								echo '<option value="'.$row['emp_id'].'">'.$row['emp_name'].'</option>';
								}
							}
							?>
						</select>
					</div>
					<div class="form-group assignid_section">
						<label for="exampleAssetsAssignID">Assign ID</label><br/>
						<input type="text" name="asset_assign_id" id="asset_assign_id" class="form-control" required autocomplete="off" value="<?php if(isset($assign_data)){echo $assign_data['asset_assign_id'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Assign ID"> 
					</div>
					<div class="form-group">
						<label for="exampleSelectAssets">Choose Assets</label>
						<select class="form-control" name="assets_name" id="assets_nameid" <?php if($_GET['view']==1){echo 'disabled';}?> >
							<option value="">Select</option>
							<?php
								$select_assets = mysqli_query($conn,"SELECT DISTINCT assets_name FROM assets WHERE assets_name != ''");
								while($row = mysqli_fetch_assoc($select_assets)){
									if($assign_data['assets_name'] == $row['assets_name']){
										echo '<option value="'.$row['assets_name'].'" selected>'.$row['assets_name'].'</option>';
									}else{
										echo '<option value="'.$row['assets_name'].'">'.$row['assets_name'].'</option>';
									}
								}
							?>
						</select><br>
					</div>
					<div class="form-group assets_brand" style="<?php if($_GET['id'] == ''){ ?>display:none<?php } ?>">
						<label for="exampleSelectAssetsbrand">Assets Brand</label>
						<select class="form-control" name="assets_brand" id="assets_brandid" <?php if($_GET['view']==1){echo 'disabled';}?> >
							<?php
								if($_GET['id']){
									$brandvalues = mysqli_query($conn,"SELECT asset_brand FROM assets WHERE assets_name = '".$assign_data['assets_name']."'");
									echo '<option value="">Select</option>';
									while($row = mysqli_fetch_assoc($brandvalues)){
										if($row['asset_brand'] == $assign_data['assets_brand']){
											echo '<option value="'.$row['asset_brand'].'" selected>'.$row['asset_brand'].'</option>';
										}else{
											echo '<option value="'.$row['asset_brand'].'">'.$row['asset_brand'].'</option>';
										}
									}
								}
							?>
						</select><br>
					</div>
					<div class="form-group assets_brand_config" style="<?php if($_GET['id'] == ''){ ?>display:none<?php } ?>">
						<label for="exampleSelectAssetsConfig">Assets Config</label><br/>
						<select class="form-control" name="assets_config" id="assets_brand_configid" <?php if($_GET['view']==1){echo 'disabled';}?> >
							<?php
								if($_GET['id']){
									$assetconfig = mysqli_query($conn,"SELECT asset_config FROM assets WHERE asset_brand = '".$assign_data['assets_brand']."'");
									echo '<option value="">Select</option>';
									while($row = mysqli_fetch_assoc($assetconfig)){
										if($row['asset_config'] == $assign_data['assets_config']){
											echo '<option value="'.$row['asset_config'].'" selected>'.$row['asset_config'].'</option>';
										}else{
											echo '<option value="'.$row['asset_config'].'">'.$row['asset_config'].'</option>';
										}
									}
								}
							?>
						</select><br>
					</div>
					<div class="form-group serialno_section" style="<?php if($_GET['id'] == ''){ ?>display:none<?php } ?>">
						<label for="serial_noid" id="add_serial_no"><?php echo $assign_data['assets_name'].' '; ?>Serial no</label><br/>
						<input type="text" name="serial_no" id="serial_noid" class="form-control" required autocomplete="off" value="<?php if(isset($assign_data)){echo $assign_data['serial_no'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Asset Serial No"> 
						<br>
					</div>
					<div class="form-group charger_serialno_section" style="<?php if($_GET['id'] == ''){ ?>display:none<?php } ?>">
						<label for="charger_serial_no">Charger Serial No.</label><br/>
						<input type="text" name="charger_serial_no" id="charger_serial_no" class="form-control" required autocomplete="off" value="<?php if(isset($assign_data)){echo $assign_data['charger_serial_no'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Charger Serial No"> 
						<br>
					</div>
					<div class="form-group dateof_purchase_section" style="<?php if($_GET['id'] == ''){ ?>display:none<?php } ?>">
						<label for="dateof_purchase">Date Of Purchase</label><br/>
						<input type="text" name="dateof_purchase" id="dateof_purchase" class="form-control" required autocomplete="off" value="<?php if(isset($assign_data)){echo $assign_data['dateof_purchase'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Date Of Purchase"> 
					</div>
					
					<?php 
						if($_GET['view']==1){
							echo '<a href="assignEmployeeList.php" class="back-cancel-btn btn btn-light">Back</a>';
						}else{
							if($_GET['id']){
								echo '<button type="submit" name="submit" class="btn btn-primary mr-2">Update</button>';
							}else{
								echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2">Submit</button>';
							}
							echo '<a href="assignEmployeeList.php" class="btn btn-light back-cancel-btn">Cancel</a>';		
						}
					?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>
  <script src="js/jquery.min.js"></script>
  <script src="js/custom.js"></script>
</body>
</html>
