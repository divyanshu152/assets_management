<?php
    session_start();
	include 'connection.php';
  if($_GET['dltid']){
    $sql = mysqli_query($conn,"DELETE FROM employee_details WHERE emp_id = ".$_GET['dltid']);
    header("location:employeeList.php");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
  <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
    <?php
      include 'sidebarmenu.php';
    ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Employee List</h4>
				  <a href="employeeForm.php">Add Employee</a>
                  <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Employee Name</th>
                          <th>Employee Code</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $sql = "SELECT * FROM `employee_details` WHERE 1";
                          $result = $conn->query($sql);
                          $slno = 1;
                          while($row=$result->fetch_assoc()) {
                            echo "<tr>
                            <td>".$slno++."</td>
                            <td>".$row["emp_name"]."</td>
                            <td>".$row["emp_code"]."</td>
                            <td><a href='employeeForm.php?id=".$row["emp_id"]."'>Edit</a> |
                            <a href='employeeForm.php?id=".$row["emp_id"]."&view=1'>View</a> |
                            <a href='employeeList.php?dltid=".$row["emp_id"]."'>Delete</a></td></tr>";
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

</body>

</html>