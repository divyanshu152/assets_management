<?php
include 'connection.php';
if(isset($_POST['hddn_id']) && $_POST['hddn_id'] != ''){//Update
	$id = $_POST['hddn_id'];
	$query = "UPDATE ms_details SET ms_id='{$_POST['msid']}',msid_password='{$_POST['msid_password']}' WHERE `id`='" . $id . "'";
	$result = mysqli_query($conn,$query);
	unset($_POST['hddn_id']);	
	header('Location:msList.php');
}elseif($_GET['id']){//Edit
	$sql = mysqli_query($conn,"SELECT * FROM ms_details WHERE id = ".$_GET['id']);
	$ms_data = mysqli_fetch_assoc($sql);
}else{//Add
	if($_POST['submit']){
		$Ms_id = $_POST['msid'];
		$msid_password = $_POST['msid_password'];
		$date = date('Y-m-d H:i:s');
		$query = "INSERT INTO `ms_details` (`ms_id`,`msid_password`,`created`) values ('{$Ms_id}','{$msid_password}','{$date}')";
		$result = mysqli_query($conn,$query);
		unset($_POST['submit']);	
		header('Location:msList.php');
	}elseif($_POST['msid']){
		$Msid = $_POST['msid'];
		$id = $_POST['id'];
		if($id != ''){
			$sql = mysqli_query($conn,"SELECT * from ms_details WHERE ms_id = '{$Msid}' AND id != '{$id}'");
		}else{			
			$sql = mysqli_query($conn,"SELECT * from ms_details WHERE ms_id ='{$Msid}'");
		}		
		$cnt_rows = $sql->num_rows;
		if($cnt_rows>=1){
			echo "1";
			exit;
		}
			
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
    <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <?php
        include 'sidebarmenu.php';
      ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
		<div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php if($_GET['view']!=1 && isset($_GET["id"])){echo 'Edit';}elseif($_GET['view']==1 && isset($_GET["id"])){echo 'View';}else{echo 'Add';}?> MS</h4>
                  <form class="forms-sample" id="form" action="msForm.php" method="post">
				  	<input type="hidden" name='hddn_id' value='<?php if($_GET['view']!=1 && isset($_GET["id"])){ echo $_GET["id"]; }; ?>'>
					<div class="form-group">
						<label for="">&nbsp;</label>Microsoft Licence<br/>
						<input type="text" name="msid" id="msid" class="form-control" value="<?php if(isset($ms_data)){echo $ms_data['ms_id'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Microsoft Licence Id">
					</div><br/>
					<div class="form-group">
						<label for="">&nbsp;</label>Password<br/>
						<input type="text" name="msid_password" id="msid_password" class="form-control" value="<?php if(isset($ms_data)){echo $ms_data['msid_password'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Licence Password">
					</div><br/>
					<?php 
						if($_GET['view']==1){
							echo '<a href="msList.php" class="back-cancel-btn btn btn-light">Back</a>';
						}else{
							if($_GET['id']){
								echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2 msid_disabled">Update</button>';
							}
							else{
								echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2 msid_disabled">Submit</button>';
							}
							echo '<a href="msList.php" class="btn btn-light back-cancel-btn">Cancel</a>';
						}
					?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

  <script>
		$(function() {
			 $(document).on("change",'#msid',function(e){ 
		                $.ajax({
		                type:"POST",
		                url: "msForm.php",
		                data: 'msid='+$('#msid').val()+'&id='+$('#id').val(),

		                success: function(data) {
		                	if(data>0) {
		                		alert('already msid is existed');
		                		$('.msid_disabled').attr('disabled',true);
		                	}else{
		                		$('.msid_disabled').removeAttr('disabled');
		                	}
						}
		            });
		        }, 
		    );
	 	});
</script>
</body>
</html>