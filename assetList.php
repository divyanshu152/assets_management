<?php
    session_start();
	  include 'connection.php';
    if($_GET['dltid']){
      $sql = mysqli_query($conn,"DELETE FROM assets WHERE id = ".$_GET['dltid']);
      header("location:assetList.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
  <?php
	  include 'header.php';
  ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
    <?php
      include 'sidebarmenu.php';
    ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Assets List</h4>
				          <a href="assetForm.php">Add Asset</a>
                  <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Assets Name</th>
                          <th>Assets Brand</th>
                          <th>Stock</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $sql = "SELECT * FROM `assets` WHERE 1";
                          $result = $conn->query($sql);
                          $slno = 1;
                          while($row=$result->fetch_assoc()) {
                            $remaining_stock = $row["stock"] - $row["in_use"];
                            echo "<tr>
                            <td>".$slno++."</td>
                            <td>".$row["assets_name"]."</td>
                            <td>".$row["asset_brand"]."</td>
                            <td>".$remaining_stock."</td>
                            <td><a href='assetForm.php?id=".$row["id"]."'>Edit</a> |
                            <a href='assetForm.php?id=".$row["id"]."&view=1'>View</a> |
                            <a href='assetList.php?dltid=".$row["id"]."'>Delete</a></td></tr>";
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
  </div>
  <!-- container-scroller -->
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

</body>

</html>