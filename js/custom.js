$(function(){
    $(document).on('change', '#assets_nameid', function() {
        if($('#assets_nameid').val() != ''){
            $.ajax({
                type: "POST",
                url: "assignEmployeeList.php",
                data:'selected_option='+$('#assets_nameid').val(),
                success: function(data){
                    $(".assets_brand").show();
                    $("#assets_brandid").html(data);
                }
            });
        }        
        $("#add_serial_no").text($('#assets_nameid').val()+' Serial No.');
        if($('#assets_nameid').val() != 'Laptop'){
            $(".charger_serialno_section").hide().val('').prop('disabled',true);
        }else{
            $(".charger_serialno_section").show();
        }
    });
    $(document).on('change', '#assets_brandid', function() {
        if($('#assets_nameid').val() != '' && $('#assets_brandid').val() != ''){
            $.ajax({
                type: "POST",
                url: "assignEmployeeList.php",
                data:'selected_name_option='+$('#assets_nameid').val()+'&selected_brand_option='+$('#assets_brandid').val(),
                success: function(data){
                    $(".assets_brand_config").show();
                    $("#assets_brand_configid").html(data);
                }
            });
        }        
    });
    $(document).on('change', '#assets_brand_configid', function() {
        if($('#assets_brand_configid').val() != ''){
            $(".serialno_section").show();
            $(".dateof_purchase_section").show();
            if($('#assets_nameid').val() == 'Laptop'){
                $(".charger_serialno_section").show();
            }
            $("#serial_noid").val('');
            $(".assignid_section").show();
            //$("#add_assign_ID").text($('#assets_nameid').val()+' Assign ID');
        }       
    });

    //Repair List
    $(document).on('change', '#repaireassets_name,#emp_name', function() {
        if($('#repaireassets_name').val().length !== 0 && $('#emp_name').val().length !== 0){
            $.ajax({
                type: "POST",
                url: "repair.php",
                data:'repaire_asset='+$('#repaireassets_name').val()+'&emp_name='+$('#emp_name').val(),
                success: function(data){
                    $('#assignasset_id').val(data);
                }
            });
        }       
    });
    /*$(document).on('click', '.add_new_assets_btn', function() {
        var a = '';
        var div_ids = $('#hddn_assets_id').val();
        div_ids++;
        $('#hddn_assets_id').val(div_ids);
        $.ajax({
            type: "POST",
            url: "assignEmployeeList.php",
            data:'assets_list=1',
            success: function(data){
                var new_record = '<div class = "assets'+div_ids+'"><div class="form-group"><label for="">&nbsp;</label>Choose Assets<br/><select name="assets_name" id="assets_nameid">'+data+'</select></div>';
                $('.assets1').after(new_record); 
            }
        });
              
    });*/
});