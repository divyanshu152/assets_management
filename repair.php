<?php
    session_start();
	  include 'connection.php';
    if($_POST['repaire_asset'] && $_POST['emp_name']){
      $assign_num = mysqli_query($conn,"SELECT asset_assign_id FROM assign_assets WHERE assets_name = '".$_POST['repaire_asset']."' AND emp_name = '".$_POST['emp_name']."' LIMIT 1");
      if(mysqli_num_rows($assign_num)>0){
        print_r(mysqli_fetch_assoc($assign_num)['asset_assign_id']);
      }exit;
    }
    if($_GET['dltid']){
      $sql = mysqli_query($conn,"DELETE FROM repair_assets WHERE repair_id = ".$_GET['dltid']);
      header("location:repair.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
  <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
    <?php
      include 'sidebarmenu.php';
    ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Repair asset List</h4>
				  <a href="repair_form.php">Add Repair asset</a>
                  <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Employee Name</th>
                          <th>Assets Name</th>
                          <th>Remark</th>
						              <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $sql = mysqli_query($conn,"SELECT a.*,e.emp_name FROM `repair_assets` as a JOIN `employee_details` as e ON a.emp_id = e.emp_id");
                          $slno=1;
                          while($row = mysqli_fetch_assoc($sql)) {
                            echo "<tr>
                            <td>".$slno++."</td>
                            <td>".$row["emp_name"]."</td>
                            <td>".$row["asset_name"]."</td>
                            <td>".$row["remark"]."</td>
                            <td><a href='repair_form.php?id=".$row["repair_id"]."'>Edit</a> |
                            <a href='repair_form.php?id=".$row["repair_id"]."&view=1'>View</a> |
                            <a href='repair.php?dltid=".$row["repair_id"]."'>Delete</a></td></tr>";
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

</body>

</html>