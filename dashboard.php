<?php
    session_start();
    include 'connection.php';
    if($_POST['submit'] == 1){
      $search_data = $_POST['search_data'];
      $select_from_employee_sql = mysqli_query($conn,"SELECT e.*,d.department_name FROM employee_details as e JOIN department_details as d ON e.emp_department = d.department_id WHERE emp_name LIKE '%".$search_data."%' AND emp_status = 1");
      $select_from_repaire_sql = mysqli_query($conn,"SELECT a.*,e.emp_name,e.emp_status FROM `repair_assets` as a JOIN `employee_details` as e ON a.emp_id = e.emp_id WHERE e.emp_name LIKE '%".$search_data."%' AND e.emp_status = 1");
      $select_from_sophos_sql = mysqli_query($conn,"SELECT * FROM sophos_list WHERE user_name LIKE '%".$search_data."%'");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
    <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <?php
        include 'sidebarmenu.php';
      ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
        <form method = "post" action="dashboard.php">
          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                  <div class="mr-md-3 mr-xl-5">
                  </div>
                    <ul class="navbar-nav mr-lg-10 w-100">
                      <li class="nav-item nav-search d-none d-lg-block w-100">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="search">
                              <i class="mdi mdi-magnify"></i>
                            </span>
                          </div>
                          <input type="text" name="search_data" class="form-control" placeholder="Search now" autocomplete="off">
                          &nbsp;&nbsp;&nbsp;<button type="submit" name="submit" value="1" class="btn btn-primary mr-2">Search</button>
                          <button class="btn btn-light">Clear</button>
                        </div>
                      </li>
                    </ul>
                </div>
              </div>
            </div>
          </div>          
        </form>
        <div class="row" style="<?php if(!$select_from_employee_sql){echo 'display:none';} ?>" >
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Employee Details</h4>
                <div class="table-responsive pt-3">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee Name</th>
                        <th>Employee Code</th>
                        <th>Department Name</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $slno = 1;
                        $emp_id = '';
                        while($row = mysqli_fetch_assoc($select_from_employee_sql)){
                          echo "<tr><td>".$slno++."</td><td>".$row["emp_name"]."</td><td>".$row["emp_code"]."</td><td>".$row["department_name"]."</td><td><a href='employeeForm.php?id=".$row["emp_id"]."'><button>edit<style=margin-right:16px;
                          width='20px';height='20px';/></a></button>
                                    <a href='employeeForm.php?id=".$row["emp_id"]."&view=1'><button>view 
                          <type='submit';width='20px';height='20px';/></button></td></tr>";
                          if($emp_id != ''){
                            $emp_id = $emp_id .','. $row['emp_id'];
                          }else{
                            $emp_id = $row['emp_id'];
                          }
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row" style="<?php if($emp_id == ''){echo 'display:none';} ?>" >
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Assign Assets Details</h4>
                <div class="table-responsive pt-3">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee Name</th>
                        <th>Asset Name</th>
                        <th>Assets Brand</th>
                        <th>Assets assign ID</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $slno = 1;
                        $select_from_assign_details_sql = mysqli_query($conn,"SELECT a.*,e.emp_name FROM assign_assets as a JOIN employee_details as e ON a.emp_name = e.emp_id WHERE a.emp_name IN (".$emp_id.")");
                        while($row = mysqli_fetch_assoc($select_from_assign_details_sql)){
                          echo "<tr><td>".$slno++."</td><td>".$row["emp_name"]."</td><td>".$row["assets_name"]."</td><td>".$row["assets_brand"]."</td><td>".$row["asset_assign_id"]."</td><td><a href='employeeForm.php?id=".$row["emp_id"]."'><button>edit<style=margin-right:16px;
                          width='20px';height='20px';/></a></button>
                                    <a href='employeeForm.php?id=".$row["emp_id"]."&view=1'><button>view 
                          <type='submit';width='20px';height='20px';/></button></td></tr>";
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row" style="<?php if($select_from_repaire_sql->num_rows < '1'){echo 'display:none';} ?>" >
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Repair assets Details</h4>
                <div class="table-responsive pt-3">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Employee Name</th>
                        <th>Assets Name</th>
                        <th>Remark</th>
						            <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $slno = 1;
                        while($row = mysqli_fetch_assoc($select_from_repaire_sql)){
                          echo "<tr>
                                <td>".$row["repair_id"]."</td>
                                <td>".$row["emp_name"]."</td>
                                <td>".$row["asset_name"]."</td>
                                <td>".$row["remark"]."</td>
                                <td><a href='repair_form.php?id=".$row["repair_id"]."'><button>edit<style=margin-right:16px;
                      width='20px';height='20px';/></a></button>
                                <a href='repair_form.php?id=".$row["repair_id"]."&view=1'><button>view 
                      <type='submit';width='20px';height='20px';/></button></td></tr>";
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row" style="<?php if($select_from_sophos_sql->num_rows < '1'){echo 'display:none';} ?>" >
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Sophos Details</h4>
                <div class="table-responsive pt-3">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $slno = 1;
                        while($row = mysqli_fetch_assoc($select_from_sophos_sql)){
                          echo "<tr><td>".$slno++."</td><td>".$row["user_name"]."</td><td><a href='sophosForm.php?id=".$row["id"]."'><button>edit<style=margin-right:16px;
                          width='20px';height='20px';/></a></button>
                                    <a href='sophosForm.php?id=".$row["id"]."&view=1'><button>view 
                          <type='submit';width='20px';height='20px';/></button></td></tr>";
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
  </div>
  <!-- container-scroller -->
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

</body>

</html>