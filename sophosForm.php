<?php
    session_start();
	include 'connection.php';
	if(isset($_POST['hddn_id']) && $_POST['hddn_id'] != ''){//Update
		$id = $_POST['hddn_id'];
		$pass_md5 = md5($_POST['password']);
		$query = "UPDATE sophos_list SET user_name='{$_POST['user_name']}',password='{$pass_md5}',view_password='{$_POST['password']}' WHERE `id`='" . $id . "'";
		$result = mysqli_query($conn,$query);
		unset($_POST['hddn_id']);	
		header('Location:sophosList.php');
	}elseif($_GET['id']){//Edit
		$sql = mysqli_query($conn,"SELECT * FROM sophos_list WHERE id = ".$_GET['id']);
		$asset_data = mysqli_fetch_assoc($sql);
	}else{//Add
		if($_POST['submit']){
			$User_name = $_POST['user_name'];
			$view_password = $_POST['password'];
			$pass_md5 = md5($_POST['password']);
			$datetime = date('Y-m-d H:i:s');
			$query = "INSERT INTO `sophos_list` (`user_name`,`password`,`view_password`,`created`) values ('{$User_name}','{$pass_md5}','{$view_password}','{$datetime}')";
			$result = mysqli_query($conn,$query);
			unset($_POST['submit']);	
			header('Location:sophosList.php');
			exit();		
		}elseif($_POST['user_name']){
			$user_name = $_POST['user_name'];
			$id = $_POST['id'];
			if($id != ''){
				$sql = mysqli_query($conn,"SELECT * from sophos_list WHERE user_name = '{$user_name}' AND id != '{$id}'");
			}else{			
				$sql = mysqli_query($conn,"SELECT * from sophos_list WHERE user_name ='{$user_name}'");
			}		
			$cnt_rows = $sql->num_rows;
			if($cnt_rows>=1){
				echo "1";
				exit;
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>AMS Admin</title>
	<link rel="stylesheet" href="css/materialdesignicons.min.css">
	<link rel="stylesheet" href="css/vendor.bundle.base.css">
	<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
	<div class="container-scroller">
		<?php
		include 'header.php';
		?>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
		<?php
			include 'sidebarmenu.php';
		?>
		<!-- partial -->
		<div class="main-panel">
			<div class="content-wrapper">
			<div class="row">
				<div class="col-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
					<h4 class="card-title"><?php if($_GET['view']!=1 && isset($_GET["id"])){echo 'Edit Sophos'; }elseif($_GET['view']==1){ echo'View Sophos'; }else{ echo'Add Sophos'; }?></h4>
					<form class="forms-sample" id="form" action="sophosForm.php" method="post">
						<input type="hidden" name='hddn_id' value='<?php if($_GET['view']!=1 && isset($_GET["id"])){ echo $_GET["id"]; }; ?>'>
						<input type="hidden" id="id" value="<?php echo($_GET['id']); ?>">
						<div class="form-group">
						<label for="user_name">User name</label>
						<input type="text" name="user_name" id="user_name" class="form-control" required value="<?php if(isset($asset_data)){echo $asset_data['user_name'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Username">
						</div>
						<div class="form-group">
						<label for="password">Password</label>
						<input type="text" name="password" id="password" class="form-control" required autocomplete="off" value="<?php if(isset($asset_data)){echo $asset_data['view_password'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Password"> 
						</div>
						<?php 
							if($_GET['view']==1){
							echo '<a href="sophosList.php">Back</a>';
							}else{
							if($_GET['id']){
								echo '<button type="submit" name="submit" class="btn btn-primary mr-2 field_sophos_disabled">Update</button>';
							}else{
								echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2 field_sophos_disabled">Submit</button>';
							}
							echo '<button class="btn btn-light">Cancel</button>';			
							}
						?>
					</form>
					</div>
				</div>
				</div>
			</div>
			</div>
			<footer class="footer">
			<div class="d-sm-flex justify-content-center justify-content-sm-between">
				<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
			</div>
			</footer>
		</div>
		</div>
	</div>
	<script src="js/vendor.bundle.base.js"></script>
	<script src="js/template.js"></script>
	<!---ajax for unique-->
	<script>
		$(function() {
		$(document).on("change",'#user_name',function(e){
					$.ajax({
						type:"POST",
						url: "sophosForm.php",
						data: 'user_name='+$('#user_name').val()+'&id='+$('#id').val(),						
						success: function(data) {//alert(data);
						if(data>0) {
							alert('Sophos name already exist');
							$('.field_sophos_disabled').attr('disabled',true);
						}else{
							$('.field_sophos_disabled').removeAttr('disabled');
						}
				}
					});
				}, 
			);
		});
	</script>
	</body>
</html>

