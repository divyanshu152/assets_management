<?php
    session_start();
include 'connection.php';

if(isset($_POST['hddn_id']) && $_POST['hddn_id'] != ''){//Update
	$id = $_POST['hddn_id'];
	$query = "UPDATE assets SET assets_name='{$_POST['assetname']}',assets_id='{$_POST['code']}',asset_brand='{$_POST['asset_brand']}',stock='{$_POST['stock']}',asset_config='{$_POST['asset_config']}' WHERE `id`='" . $id . "'";
	$result = mysqli_query($conn,$query);
	unset($_POST['hddn_id']);	
	header('Location:assetList.php');
}elseif($_GET['id']){//Edit
	$sql = mysqli_query($conn,"SELECT * FROM assets WHERE id = ".$_GET['id']);
	$asset_data = mysqli_fetch_assoc($sql);
}else{//Add
	if($_POST['submit']){
		$asset_name = $_POST['assetname'];
		$asset_code = $_POST['code'];
		$asset_brand = $_POST['asset_brand'];
		$asset_stock = $_POST['stock'];
		$asset_config = $_POST['asset_config'];
		$query = "INSERT INTO `assets` (`assets_name`, `assets_id`, `asset_brand`, `stock`, `asset_config`) values ('{$asset_name}','{$asset_code}','{$asset_brand}','{$asset_stock}','{$asset_config}')";
		$result = mysqli_query($conn,$query);
		unset($_POST['submit']);	
		header('Location:assetList.php');
	}
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
    <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <?php
        include 'sidebarmenu.php';
      ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
		<div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php if($_GET['view']!=1 && isset($_GET["id"])){echo 'Edit Asset'; }elseif($_GET['view']==1){ echo'Edit Asset'; }else{ echo'Add New Asset'; }?></h4>
                  <form class="forms-sample" id="form" action="assetForm.php" method="post">
					<input type="hidden" name='hddn_id' value='<?php if($_GET['view']!=1 && isset($_GET["id"])){ echo $_GET["id"]; }; ?>'>
                    <div class="form-group">
                      <label for="assetname">Asset name</label>
                      <input type="text" name="assetname" id="assetname" class="form-control" required value="<?php if(isset($asset_data)){echo $asset_data['assets_name'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Asset Name">
                    </div>
                    <!--div class="form-group">
                      <label for="assetno">Asset No</label>
					  <input type="text" name="code" id="code" class="form-control" required autocomplete="off" value="<?php if(isset($asset_data)){echo $asset_data['assets_id'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Asset Number"> 
                    </div-->
                    <div class="form-group">
                      <label for="exampleInputPassword4">Brand</label>
                      <input type="text" name="asset_brand" id="asset_brand" class="form-control" required autocomplete="off" value="<?php if(isset($asset_data)){echo $asset_data['asset_brand'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Asset Brand"> 
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword4">Stock</label>
                      <input type="text" name="stock" id="asset_stock" class="form-control" required autocomplete="off" value="<?php if(isset($asset_data)){echo $asset_data['stock'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Stock"> 
                    </div>
                    <?php if($_GET['view']==1 || isset($asset_data)){ ?>
                      <div class="form-group">
                        <label for="exampleInputPassword4">In USE</label>
                        <input type="text" class="form-control" autocomplete="off" value="<?php if(isset($asset_data) && isset($asset_data['in_use'])){echo $asset_data['in_use'];}else{echo '0';} ?>" disabled placeholder="In use Stock"> 
                      </div>
                    <?php } ?>
                    <div class="form-group">
                      <label for="exampleTextarea1">Configuration</label>
					  <textarea name="asset_config" class="form-control" <?php if($_GET['view']==1){echo 'disabled';}?>  rows="4"  placeholder="Asset Configuration"><?php if(isset($asset_data)){echo $asset_data['asset_config'];} ?></textarea>
                    </div>
					<?php 
						if($_GET['view']==1){
							echo '<a href="assetList.php">Back</a>';
						}else{
							if($_GET['id']){
								echo '<button type="submit" name="submit" class="btn btn-primary mr-2">Update</button>';
							}else{
								echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2">Submit</button>';
							}
							echo '<button class="btn btn-light">Cancel</button>';			
						}
					?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

</body>

</html>