<?php
include 'connection.php';
if(isset($_POST['hddn_id']) && $_POST['hddn_id'] != ''){//Update
	$id = $_POST['hddn_id'];
	$query = "UPDATE employee_details SET emp_name='{$_POST['emp_name']}',emp_code='{$_POST['emp_code']}',emp_department='{$_POST['emp_department']}',emp_status='{$_POST['emp_status']}' WHERE `emp_id`='" . $id . "'";
	$result = mysqli_query($conn,$query);
	unset($_POST['hddn_id']);	
	header('Location:employeeList.php');
}elseif($_GET['id']){//Edit
	$sql = mysqli_query($conn,"SELECT * FROM employee_details WHERE emp_id = ".$_GET['id']);
	$emp_data = mysqli_fetch_assoc($sql);
}else{//Add
	if($_POST['submit']){
		$emp_name = $_POST['emp_name'];
		$emp_code = $_POST['emp_code'];
		$emp_department = $_POST['emp_department'];
    $emp_status = $_POST['emp_status'];
    $datetime = date("Y-m-d H:i:s");
		$query = "INSERT INTO `employee_details` (`emp_name`, `emp_code`, `emp_department`, `emp_status`, `created_at`) values ('{$emp_name}','{$emp_code}','{$emp_department}','{$emp_status}','{$datetime}')";
    $result = mysqli_query($conn,$query);
		unset($_POST['submit']);	
		header('Location:employeeList.php');
	}
  elseif($_POST['emp_name']){
    $emp_name = $_POST['emp_name'];
   $sql = mysqli_query($conn,"SELECT * from employee_details WHERE emp_name='{$emp_name}'");
    $cnt_rows = $sql->num_rows;
    print_r($cnt_rows);exit;
  }
  elseif($_POST['emp_code']){
    $emp_code = $_POST['emp_code'];
   $sql = mysqli_query($conn,"SELECT * from employee_details WHERE emp_code='{$emp_code}'");
    $cnt_rows = $sql->num_rows;
    print_r($cnt_rows);exit;
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
  <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
    <?php
      include 'sidebarmenu.php';
    ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
		      <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add New Employee</h4>
                  <form class="forms-sample" id="form" action="employeeForm.php" method="post">
					          <input type="hidden" name='hddn_id' value='<?php if($_GET['view']!=1 && isset($_GET["id"])){ echo $_GET["id"]; }; ?>'>
                    <div class="form-group">
                      <label for="empname">Employee name</label>
                      <input type="text" name="emp_name" id="emp_name" class="form-control" required value="<?php if(isset($emp_data)){echo $emp_data['emp_name'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Employee Name">
                    </div>
                    <div class="form-group">
                      <label for="empcode">Employee Code</label>
					            <input type="text" name="emp_code" id="emp_code" class="form-control" required autocomplete="off" value="<?php if(isset($emp_data)){echo $emp_data['emp_code'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Employee code"> 
                    </div>
					          <div class="form-group">
                      <label for="exampleSelectDepartment">Department</label>
                      <select class="form-control" name="emp_department" id="emp_department"  <?php if($_GET['view']==1){echo 'disabled';}?>>
                      <?php
                        $select_depart = mysqli_query($conn,"SELECT * FROM department_details WHERE 1");
                        while($row = mysqli_fetch_assoc($select_depart)){
                          if($emp_data['emp_department'] == $row['department_id']){
                            echo '<option value="'.$row['department_id'].'" selected>'.$row['department_name'].'</option>';
                          }else{
                            echo '<option value="'.$row['department_id'].'">'.$row['department_name'].'</option>';
                          }
                        }
                      ?>
                      </select>
                    </div>
                    <div class="form-group">
                    <label for="status">Employee Status</label>
                    <div class="col-sm-4">
                    <div class="form-check">
                      <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="emp_status" id="emp_status" value="1" <?php if($emp_data['emp_status']==1){echo 'checked';}else{echo 'checked';}?>  <?php if($_GET['view']==1){echo 'disabled';}?>>
                      Enabled
                      <i class="input-helper"></i></label>
                    </div>
                    </div>
                    <div class="col-sm-5">
                    <div class="form-check">
                      <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="emp_status" id="emp_status" value="2" <?php if($emp_data['emp_status']==2){echo 'checked';}?>  <?php if($_GET['view']==1){echo 'disabled';}?>>
                      Disabled
                      <i class="input-helper"></i></label>
                    </div>
                    </div>
                  </div>
                  <?php 
                    if($_GET['view']==1){
                      echo '<a href="employeeList.php">Back</a>';
                    }else{
                      if($_GET['id']){
                        echo '<button type="submit" name="submit" class="btn btn-primary mr-2 field_disabled">Update</button>';
                      }else{
                        echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2 field_disabled">Submit</button>';
                      }
                      echo '<button class="btn btn-light">Cancel</button>';			
                    }
                  ?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

  <!--ajax code for unique-->
  <script>
    $(function() {
       $(document).on("change",'#emp_name',function(e){
                $.ajax({
                    type:"POST",
                    url: "employeeForm.php",
                    data: 'emp_name='+$('#emp_name').val(),
                    success: function(data) {
                      if(data>0) {
                        alert('already empname is existed');
                        $('.field_disabled').attr('disabled',true);
                      }else{
                        $('.field_disabled').removeAttr('disabled');
                      }
            }
                });
            }, 
        );
    });
</script>

<!--ajax code for empcode-->
  <script>
    $(function() {
       $(document).on("change",'#emp_code',function(e){
                $.ajax({
                    type:"POST",
                    url: "employeeForm.php",
                    data: 'emp_code='+$('#emp_code').val(),
                    success: function(data) {
                      if(data>0) {
                        alert('already Employee code is existed');
                        $('field_name.disabled').attr('disabled',true);
                      }else{
                        $('field.disabled').removeAttr('disabled');
                      }
            }
                });
            }, 
        );
    });
</script>






</body>

</html>