<?php

include('connection.php');
session_start();
if(($_SESSION['loggedUser'] != '1') && (isset($_POST['uname']) && isset($_POST['psw']))){
    session_destroy();
    $uname = $_POST['uname'];
    $password = md5($_POST['psw']);
    $usersql = mysqli_query($conn,"SELECT * FROM admin_users WHERE username='".$uname."' AND password='".$password."' AND super_admin = '1'");
    if($usersql->num_rows){
        session_start();
        $_SESSION['loggedUser'] = '1';
        $_SESSION['username'] = mysqli_fetch_assoc($usersql)['name'];
        header('Location:index.php');
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
              </div>
              <!--h4>Hello! let's get started</h4-->
              <h6 class="font-weight-light">Sign in to continue.</h6>
              <form class="pt-3" action="userLogin.php" method="post">
                <div class="form-group">
                  <input type="text" name="uname" class="form-control form-control-lg" autocomplete="off" placeholder="Username" required>
                </div>
                <div class="form-group">
                  <input type="password" name="psw" class="form-control form-control-lg" autocomplete="off" placeholder="Password" required>
                </div>
                <div class="mt-3">
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">SIGN IN</button>
                </div>
                <!--div class="my-2 d-flex justify-content-between align-items-center">
                  <a href="#" class="auth-link text-black">Forgot password?</a>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  Don't have an account? <a href="register.html" class="text-primary">Create</a>
                </div-->
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>