<?php
    session_start();
	include 'connection.php';
  if($_GET['dltid']){
    $sql = mysqli_query($conn,"DELETE FROM department_details WHERE department_id = ".$_GET['dltid']);
    header("location:departmentList.php");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
    <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <?php
        include 'sidebarmenu.php';
      ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Department List</h4>
				  <a href="departmentForm.php">Add Department</a>
                  <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Department name</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $sql = "SELECT * FROM `department_details` WHERE 1";
                          $result = $conn->query($sql);
                          $slno = 1;
                          while($row=$result->fetch_assoc()) {
                            echo "<tr>
                            <td>".$slno++."</td>
                            <td>".$row["department_name"]."</td>
                            <td><a href='departmentForm.php?id=".$row["department_id"]."'>Edit</a> |
                                <a href='departmentForm.php?id=".$row["department_id"]."&view=1'>View</a> |
                                <a href='departmentList.php?dltid=".$row["department_id"]."'>Delete</a>
                                </td></tr>";
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

</body>
</html>