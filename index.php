<?php

session_start();
if($_GET['logout'] == 1){
    unset($_SESSION['loggedUser']);
    unset($_SESSION['username']);    
    session_destroy();
    header('Location:index.php');
}else{
    if($_SESSION['loggedUser'] =='' ){
        header('Location:userLogin.php');
    }else{
        header('Location:dashboard.php');
    }
}

?>