<?php
include 'connection.php';

if(isset($_POST['hddn_id']) && $_POST['hddn_id'] != ''){//Update
	$id = $_POST['hddn_id'];
	$query = "UPDATE department_details SET department_name='{$_POST['department_name']}' WHERE `department_id`='" . $id . "'";
	$result = mysqli_query($conn,$query);
	unset($_POST['hddn_id']);	
	header('Location:departmentList.php');
}elseif($_GET['id']){//Edit
	$sql = mysqli_query($conn,"SELECT * FROM department_details WHERE department_id = ".$_GET['id']);
	$department_data = mysqli_fetch_assoc($sql);
}else{//Add
	if($_POST['submit']){
		$department_name = $_POST['department_name'];
		$query = "INSERT INTO `department_details` (`department_name`) values ('{$department_name}')";
		$result = mysqli_query($conn,$query);
		unset($_POST['submit']);	
		header('Location:departmentList.php');
	}
  elseif($_POST['department_name']){
    $department_name = $_POST['department_name'];
    $sql = mysqli_query($conn,"SELECT * from department_details WHERE department_name='{$department_name}'");
    $cnt_rows = $sql->num_rows;
    print_r($cnt_rows);exit;
  }
  
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
    <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
    <?php
      include 'sidebarmenu.php';
    ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
		      <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php if($_GET['view']!=1 && isset($_GET["id"])){echo 'Edit';}elseif($_GET['view']==1 && isset($_GET["id"])){echo 'View';}else{echo 'Add';}?> Department</h4>
                  <form class="forms-sample" id="form" action="departmentForm.php" method="post">
					          <input type="hidden" name='hddn_id' value='<?php if($_GET['view']!=1 && isset($_GET["id"])){ echo $_GET["id"]; }; ?>'>
                    <div class="form-group">
                      <label for="department_name">Department name</label><br/>
                      <input type="text" name="department_name" id="department_name" class="form-control" required value="<?php if(isset($department_data)){echo $department_data['department_name'];} ?>" <?php if($_GET['view']==1){echo 'disabled';}?> placeholder="Department Name">
                    </div><br/>
                    <?php 
                      if($_GET['view']==1){
                        echo '<a href="departmentList.php" class="back-cancel-btn btn btn-light">Back</a>';
                      }else{
                        if($_GET['id']){
                          echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2 department_name_disabled">Update</button>';
                        }else{
                          echo '<button type="submit" name="submit" value="1" class="btn btn-primary mr-2 department_name_disabled">Submit</button>';
                        }
                        echo '<a href="departmentList.php" class="btn btn-light back-cancel-btn">Cancel</a>';
                      }
                    ?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

  <!---ajax for unique-->
  <script>
    $(function() {
       $(document).on("change",'#department_name',function(e){
                $.ajax({
                    type:"POST",
                    url: "departmentForm.php",
                    data: 'department_name='+$('#department_name').val(),
                    success: function(data) {
                      if(data>0) {
                        alert('Department name already exists');
                        $('.department_name_disabled').attr('disabled',true);
                      }else{
                        $('.department_name_disabled').removeAttr('disabled');
                      }
            }
                });
            }, 
        );
    });
</script>


</body>

</html>

