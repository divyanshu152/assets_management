<?php
	session_start();
	include 'connection.php';
	if($_POST['selected_option']){
		$brandvalues = mysqli_query($conn,"SELECT asset_brand FROM assets WHERE assets_name = '".$_POST['selected_option']."'");
		echo '<option value="">Select</option>';
		while($row = mysqli_fetch_assoc($brandvalues)){
			echo '<option>';
			echo $row['asset_brand'];
			echo '</option>';
		}exit;
	}elseif($_POST['selected_name_option'] && $_POST['selected_brand_option']){
		$assetconfig = mysqli_query($conn,"SELECT asset_config FROM assets WHERE assets_name = '".$_POST['selected_name_option']."' AND asset_brand = '".$_POST['selected_brand_option']."'");
		echo '<option value="">Select</option>';
		while($row = mysqli_fetch_assoc($assetconfig)){
			echo '<option >';
			echo $row['asset_config'];
			echo '</option>';
		}exit;
	}elseif($_POST['assets_list']){
		$brandvalues = mysqli_query($conn,"SELECT DISTINCT assets_name FROM assets WHERE assets_name != ''");
		echo '<option value="">Select</option>';
		while($row = mysqli_fetch_assoc($brandvalues)){
			echo '<option>';
			echo $row['assets_name'];
			echo '</option>';
		}exit;
	}
  if($_GET['dltid']){
    $sql = mysqli_query($conn,"DELETE FROM assign_assets WHERE assign_id = ".$_GET['dltid']);
    header("location:assignEmployeeList.php");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>AMS Admin</title>
  <link rel="stylesheet" href="css/materialdesignicons.min.css">
  <link rel="stylesheet" href="css/vendor.bundle.base.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container-scroller">
    <?php
      include 'header.php';
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
    <?php
      include 'sidebarmenu.php';
    ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                  <!--search-->
                  <?php
    session_start();
    include 'connection.php';
      $search_data = $_POST['search_data'];
      $select_from_assign_details_sql = mysqli_query($conn,"SELECT a.*,e.emp_name FROM assign_assets as a JOIN employee_details as e ON a.emp_name = e.emp_id WHERE a.emp_name IN (".$emp_id.")");
     
  
?>
      <!-- partial -->
        <form method = "post" action="assignEmployeeList.php">
          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                  <div class="mr-md-3 mr-xl-5">
                  </div>
                    <ul class="navbar-nav mr-lg-10 w-100">
                      <li class="nav-item nav-search d-none d-lg-block w-100">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="search">
                              <i class="mdi mdi-magnify"></i>
                            </span>
                          </div>
                          <input type="text" name="search_data" class="form-control" placeholder="Search now" autocomplete="off">
                          &nbsp;&nbsp;&nbsp;<button type="submit" name="submit" value="1" class="btn btn-primary mr-2">Search</button>
                          <button class="btn btn-light">Clear</button>
                        </div>
                      </li>
                    </ul>
                </div>
              </div>
            </div>
          </div>          
        </form>



                        <?php
                          $search_data = $_POST['search_data'];
      			$select_from_assign_details_sql = mysqli_query($conn,"SELECT a.*,e.emp_name,e.emp_id FROM employee_details as e JOIN assign_assets as a ON a.emp_name = e.emp_id WHERE e.emp_name LIKE '%".$search_data."%' AND emp_status = 1");
	?>
                  <h4 class="card-title">Assign List</h4>
                  <a href="assignEmployeeForm.php">Assign assets</a>
                  <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Employee name</th>
                          <th>Asset name</th>
                          <th>Asset brand</th>
			                    <th>Serial Number</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 
                        if(isset($_POST['search_data'])) {
                          $search_data = $_POST['search_data'];
                          $select_from_assign_details_sql = mysqli_query($conn,"SELECT a.*,e.emp_name,e.emp_id FROM employee_details as e JOIN assign_assets as a ON a.emp_name = e.emp_id WHERE e.emp_name LIKE '%".$search_data."%' AND emp_status = 1");
                          $rows = $select_from_assign_details_sql->num_rows;
                          $slno = 1;
                          unset($_POST['search_data']);
                          if($rows>0){
                              while($row = mysqli_fetch_assoc($select_from_assign_details_sql)){
                                echo "<tr><td>".$slno++."</td><td>".$row["emp_name"]."</td><td>".$row["assets_name"]."</td><td>".$row["assets_brand"]."</td><td>".$row["asset_assign_id"]."</td><td><a href='assignEmployeeForm.php?id=".$row["emp_id"]."'>Edit</a> |
                                <a href='assignEmployeeForm.php?id=".$row["assign_id"]."&view=1'>View</a> |
                                <a href='assignEmployeeList.php?dltid=".$row["assign_id"]."'>Delete </a></td></tr>";
                              }
                          } else{
                            echo "<tr>";
                            echo "<td colspan='6' style='text-align: center;'>No record found</td>";
                            echo "</tr>";
                          }
                        }else{
                            $sql = "SELECT a.*,e.emp_name FROM `assign_assets` as a JOIN `employee_details` as e ON a.emp_name = e.emp_id ORDER BY a.assign_id";
                            $result = $conn->query($sql);
                            $slno = 1;
                            while($row=$result->fetch_assoc()) {
                              echo "<tr><td>".$slno++."</td><td>".$row["emp_name"]."</td><td>".$row["assets_name"]."</td><td>".$row["assets_brand"]."</td><td>".$row["serial_no"]."</td><td><a href='assignEmployeeForm.php?id=".$row["assign_id"]."'>Edit</a> |
                              <a href='assignEmployeeForm.php?id=".$row["assign_id"]."&view=1'>View</a> |
                              <a href='assignEmployeeList.php?dltid=".$row["assign_id"]."'>Delete </a></td></tr>";
                            }
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © AMS 2020</span>
          </div>
        </footer>
      </div>
    </div>
  </div>
  <script src="js/vendor.bundle.base.js"></script>
  <script src="js/template.js"></script>

</body>

</html>